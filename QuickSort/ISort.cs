﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickSort
{
    interface ISort<T> where T : IComparable
    {
        void Sort(T[] data);
    }
}
