﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickSort
{
    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch sw = new Stopwatch();
            Random rnd = new Random();

            int[] sourceData = new int[50000000];

            for (int i = 0; i < sourceData.Length; i++)
            {
                sourceData[i] = rnd.Next(1, 10000);
            }

            Console.WriteLine("Type 1 for Sequential, 2 for Parallel");
            ConsoleKeyInfo key = Console.ReadKey();

            sw.Start();
            ISort<int> sort = null;

            switch (key.KeyChar)
            {
                case '1':
                    sort = new QuickSort<int>();
                    break;
                case '2':
                    sort = new ParallelQuickSort<int>();
                    break;
                default:
                    Console.WriteLine("Invalid Selection, Press any key to exit");
                    Console.ReadKey();
                    return;
            }

            if (sort != null)
            {
                Console.WriteLine();
                Console.WriteLine("Sorting...");
                sort.Sort(sourceData);
            }

            sw.Stop();

            //Prints every 10000th result
            for (int i = 0; i < sourceData.Length; i += 10000)
            {
                Console.WriteLine("sourceData[" + i + "] : " + sourceData[i]);
            }

            Console.WriteLine("Time Taken for QuickSort: " + sw.ElapsedMilliseconds + "ms");
            Console.ReadKey();
        }
    }
}
